#ifndef ANNOTATEAPP_H
#define ANNOTATEAPP_H

#include <QEvent>
#include <QFile>
#include <QFileDialog>
#include <QInputDialog>
#include <QLineEdit>
#include <QKeyEvent>
#include <QMainWindow>
#include <QMessageBox>
#include <QRubberBand>
#include <QTextStream>

namespace Ui
{
    class AnnotateApp;
}

const int STATUSBAR_TIMEOUT = 5000;

class AnnotateApp : public QMainWindow
{
    Q_OBJECT

    public:
        explicit AnnotateApp(QWidget *parent = 0);
        ~AnnotateApp();

       void keyPressEvent(QKeyEvent *event);
       void wheelEvent(QWheelEvent *event);
       void mousePressEvent(QMouseEvent *event);
       void mouseMoveEvent(QMouseEvent *event);
       void mouseReleaseEvent(QMouseEvent *event);

       bool eventFilter(QObject *obj, QEvent *event);

    protected slots:
        void on_cmdAddClass_clicked();
        void on_cmdRemoveClass_clicked();
        void on_actionLoad_Directory_triggered();
        void on_actionSave_Classes_triggered();
        void on_actionSave_Classes_As_triggered();
        void on_actionSave_Session_triggered();
        void on_actionSave_Session_As_triggered();
        void on_classList_itemSelectionChanged();
        void on_imageList_itemSelectionChanged();
        void on_imageProgress_valueChanged(int value);

    private:
        void writeClasses(const QString& fileName);

        Ui::AnnotateApp *ui;
        QString currentFolder;
        QString classFile;
        QString sessionFile;

        QRubberBand *rubberBand;
        QPoint origin;
};

#endif // ANNOTATEAPP_H
