/********************************************************************************
** Form generated from reading UI file 'AnnotateApp.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ANNOTATEAPP_H
#define UI_ANNOTATEAPP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AnnotateApp
{
public:
    QAction *actionLoad_Directory;
    QAction *actionSave_Session;
    QAction *actionQuit;
    QAction *actionSave_Classes;
    QAction *actionSave_Classes_As;
    QAction *actionSave_Session_As;
    QWidget *centralwidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QLabel *image;
    QPushButton *cmdAddClass;
    QPushButton *cmdRemoveClass;
    QScrollBar *imageProgress;
    QTableWidget *imageList;
    QTableWidget *classList;
    QMenuBar *menubar;
    QMenu *menuFile;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *AnnotateApp)
    {
        if (AnnotateApp->objectName().isEmpty())
            AnnotateApp->setObjectName(QStringLiteral("AnnotateApp"));
        AnnotateApp->resize(810, 600);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(AnnotateApp->sizePolicy().hasHeightForWidth());
        AnnotateApp->setSizePolicy(sizePolicy);
        AnnotateApp->setFocusPolicy(Qt::WheelFocus);
        actionLoad_Directory = new QAction(AnnotateApp);
        actionLoad_Directory->setObjectName(QStringLiteral("actionLoad_Directory"));
        actionSave_Session = new QAction(AnnotateApp);
        actionSave_Session->setObjectName(QStringLiteral("actionSave_Session"));
        actionQuit = new QAction(AnnotateApp);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionSave_Classes = new QAction(AnnotateApp);
        actionSave_Classes->setObjectName(QStringLiteral("actionSave_Classes"));
        actionSave_Classes_As = new QAction(AnnotateApp);
        actionSave_Classes_As->setObjectName(QStringLiteral("actionSave_Classes_As"));
        actionSave_Session_As = new QAction(AnnotateApp);
        actionSave_Session_As->setObjectName(QStringLiteral("actionSave_Session_As"));
        centralwidget = new QWidget(AnnotateApp);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        sizePolicy.setHeightForWidth(centralwidget->sizePolicy().hasHeightForWidth());
        centralwidget->setSizePolicy(sizePolicy);
        gridLayout_2 = new QGridLayout(centralwidget);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        image = new QLabel(centralwidget);
        image->setObjectName(QStringLiteral("image"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(10);
        sizePolicy1.setVerticalStretch(3);
        sizePolicy1.setHeightForWidth(image->sizePolicy().hasHeightForWidth());
        image->setSizePolicy(sizePolicy1);
        image->setFrameShape(QFrame::Box);
        image->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(image, 0, 2, 3, 1);

        cmdAddClass = new QPushButton(centralwidget);
        cmdAddClass->setObjectName(QStringLiteral("cmdAddClass"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(cmdAddClass->sizePolicy().hasHeightForWidth());
        cmdAddClass->setSizePolicy(sizePolicy2);

        gridLayout->addWidget(cmdAddClass, 1, 0, 1, 1);

        cmdRemoveClass = new QPushButton(centralwidget);
        cmdRemoveClass->setObjectName(QStringLiteral("cmdRemoveClass"));
        cmdRemoveClass->setEnabled(false);

        gridLayout->addWidget(cmdRemoveClass, 1, 1, 1, 1);

        imageProgress = new QScrollBar(centralwidget);
        imageProgress->setObjectName(QStringLiteral("imageProgress"));
        sizePolicy2.setHeightForWidth(imageProgress->sizePolicy().hasHeightForWidth());
        imageProgress->setSizePolicy(sizePolicy2);
        imageProgress->setMinimumSize(QSize(0, 20));
        imageProgress->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(imageProgress, 3, 2, 1, 1);

        imageList = new QTableWidget(centralwidget);
        if (imageList->columnCount() < 2)
            imageList->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setTextAlignment(Qt::AlignCenter);
        imageList->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        __qtablewidgetitem1->setText(QStringLiteral("#ROIs"));
        __qtablewidgetitem1->setTextAlignment(Qt::AlignCenter);
        imageList->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        imageList->setObjectName(QStringLiteral("imageList"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(1);
        sizePolicy3.setVerticalStretch(5);
        sizePolicy3.setHeightForWidth(imageList->sizePolicy().hasHeightForWidth());
        imageList->setSizePolicy(sizePolicy3);
        imageList->setContextMenuPolicy(Qt::NoContextMenu);
        imageList->setAutoScroll(false);
        imageList->setEditTriggers(QAbstractItemView::NoEditTriggers);
        imageList->setProperty("showDropIndicator", QVariant(false));
        imageList->setDragDropOverwriteMode(false);
        imageList->setSelectionMode(QAbstractItemView::SingleSelection);
        imageList->setSelectionBehavior(QAbstractItemView::SelectRows);
        imageList->setWordWrap(false);
        imageList->setCornerButtonEnabled(false);
        imageList->setRowCount(0);
        imageList->setColumnCount(2);
        imageList->horizontalHeader()->setStretchLastSection(true);
        imageList->verticalHeader()->setVisible(false);
        imageList->verticalHeader()->setHighlightSections(false);

        gridLayout->addWidget(imageList, 2, 0, 2, 2);

        classList = new QTableWidget(centralwidget);
        if (classList->columnCount() < 1)
            classList->setColumnCount(1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        __qtablewidgetitem2->setTextAlignment(Qt::AlignCenter);
        classList->setHorizontalHeaderItem(0, __qtablewidgetitem2);
        classList->setObjectName(QStringLiteral("classList"));
        sizePolicy.setHeightForWidth(classList->sizePolicy().hasHeightForWidth());
        classList->setSizePolicy(sizePolicy);
        classList->setContextMenuPolicy(Qt::NoContextMenu);
        classList->setAutoScroll(false);
        classList->setEditTriggers(QAbstractItemView::NoEditTriggers);
        classList->setProperty("showDropIndicator", QVariant(false));
        classList->setDragDropOverwriteMode(false);
        classList->setDefaultDropAction(Qt::IgnoreAction);
        classList->setSelectionMode(QAbstractItemView::SingleSelection);
        classList->setSelectionBehavior(QAbstractItemView::SelectRows);
        classList->setWordWrap(false);
        classList->setCornerButtonEnabled(false);
        classList->setRowCount(0);
        classList->setColumnCount(1);
        classList->horizontalHeader()->setVisible(true);
        classList->horizontalHeader()->setHighlightSections(true);
        classList->horizontalHeader()->setStretchLastSection(true);
        classList->verticalHeader()->setVisible(false);
        classList->verticalHeader()->setHighlightSections(false);

        gridLayout->addWidget(classList, 0, 0, 1, 2);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        AnnotateApp->setCentralWidget(centralwidget);
        menubar = new QMenuBar(AnnotateApp);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 810, 25));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        AnnotateApp->setMenuBar(menubar);
        statusbar = new QStatusBar(AnnotateApp);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        statusbar->setSizeGripEnabled(true);
        AnnotateApp->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menuFile->addAction(actionLoad_Directory);
        menuFile->addSeparator();
        menuFile->addAction(actionSave_Classes);
        menuFile->addAction(actionSave_Classes_As);
        menuFile->addSeparator();
        menuFile->addAction(actionSave_Session);
        menuFile->addAction(actionSave_Session_As);
        menuFile->addSeparator();
        menuFile->addAction(actionQuit);

        retranslateUi(AnnotateApp);
        QObject::connect(actionQuit, SIGNAL(triggered()), AnnotateApp, SLOT(close()));

        QMetaObject::connectSlotsByName(AnnotateApp);
    } // setupUi

    void retranslateUi(QMainWindow *AnnotateApp)
    {
        AnnotateApp->setWindowTitle(QApplication::translate("AnnotateApp", "Image Annotator", Q_NULLPTR));
        actionLoad_Directory->setText(QApplication::translate("AnnotateApp", "Load Directory (Ctrl+L)", Q_NULLPTR));
        actionSave_Session->setText(QApplication::translate("AnnotateApp", "Save Session (Ctrl+S)", Q_NULLPTR));
        actionQuit->setText(QApplication::translate("AnnotateApp", "Quit", Q_NULLPTR));
        actionSave_Classes->setText(QApplication::translate("AnnotateApp", "Save Classes (Ctrl+C)", Q_NULLPTR));
        actionSave_Classes_As->setText(QApplication::translate("AnnotateApp", "Save Classes As (Ctrl+Shift+C)", Q_NULLPTR));
        actionSave_Session_As->setText(QApplication::translate("AnnotateApp", "Save Session As (Ctrl+Shift+S)", Q_NULLPTR));
        image->setText(QString());
        cmdAddClass->setText(QApplication::translate("AnnotateApp", "Add Class", Q_NULLPTR));
        cmdRemoveClass->setText(QApplication::translate("AnnotateApp", "Remove Class", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem = imageList->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("AnnotateApp", "Image", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem1 = classList->horizontalHeaderItem(0);
        ___qtablewidgetitem1->setText(QApplication::translate("AnnotateApp", "Class", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("AnnotateApp", "File", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AnnotateApp: public Ui_AnnotateApp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ANNOTATEAPP_H
