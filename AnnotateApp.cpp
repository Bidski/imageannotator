#include "AnnotateApp.h"
#include "ui_AnnotateApp.h"

AnnotateApp::AnnotateApp(QWidget *parent) : QMainWindow(parent), ui(new Ui::AnnotateApp), rubberBand(nullptr)
{
    ui->setupUi(this);

    // Install an event filter to prevent keypress events from being handled by the QTableWidgets.
    ui->classList->installEventFilter(this);
    ui->imageList->installEventFilter(this);

    ui->imageProgress->setRange(0, 0);
    ui->imageProgress->setValue(0);
}

AnnotateApp::~AnnotateApp()
{
    delete ui;
}

void AnnotateApp::on_cmdAddClass_clicked()
{
    bool ok, flag = true;
    QString newClass;

    do
    {
        flag = true;

        QString text = QInputDialog::getText(this, "Add A New Class ...", "Class Name:", QLineEdit::Normal, "NEW_CLASS", &ok);

        if (ok && !text.isEmpty())
        {
            newClass = text.toUpper().replace(" ", "_").replace("__", "_").trimmed();

            // Prevent duplicates from being added.
            for (int row = 0; row < ui->classList->rowCount(); row++)
            {
                QString temp = ui->classList->item(row, 0)->text();
                if (temp.compare(newClass) == 0)
                {
                    QMessageBox::warning(this, "Duplicate Class", QString("Class '%1' already exists. Please choose another.").arg(newClass));
                    flag = false;
                    newClass.clear();
                }
            }
        }
    } while (flag == false);

    if (!newClass.isNull())
    {
        ui->classList->insertRow(ui->classList->rowCount());
        ui->classList->setItem(ui->classList->rowCount() - 1, 0, new QTableWidgetItem(newClass));
        ui->statusbar->showMessage(QString("Added class '%1'.").arg(newClass), STATUSBAR_TIMEOUT);
    }
}

void AnnotateApp::on_cmdRemoveClass_clicked()
{
    if (ui->classList->currentRow() >= 0)
    {
        QString item = ui->classList->currentItem()->text();
        QString prompt = QString("Confirm deletion of class '%1'?").arg(item);

        if (QMessageBox::question(this, "Delete Class?", prompt, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
        {
            ui->classList->removeRow(ui->classList->currentRow());
            ui->statusbar->showMessage(QString("Removed class '%1'.").arg(item), STATUSBAR_TIMEOUT);
        }
    }
}

void AnnotateApp::on_actionLoad_Directory_triggered()
{
    currentFolder = QFileDialog::getExistingDirectory(this, "Load Image Directory", QDir::homePath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    currentFolder.append("/");

    ui->statusbar->showMessage(QString("Loading images from %1.").arg(currentFolder), STATUSBAR_TIMEOUT);

    QStringList filters;
    filters << "*.jpg" << "*.jpeg" << "*.png" << "*.bmp";

    QDir images(currentFolder, "", QDir::Name|QDir::IgnoreCase, QDir::Files|QDir::Readable);
    images.setNameFilters(filters);

    QStringList files = images.entryList(QDir::Files|QDir::Readable, QDir::Name|QDir::IgnoreCase);

    for (int i = 0; i < files.size(); i++)
    {
        ui->statusbar->showMessage(QString("Loading image %1 of %2: %3").arg(i + 1).arg(files.size()).arg(files.at(i)), STATUSBAR_TIMEOUT);

        ui->imageList->insertRow(ui->imageList->rowCount());
        ui->imageList->setItem(ui->imageList->rowCount() - 1, 0, new QTableWidgetItem(files.at(i)));
        ui->imageList->setItem(ui->imageList->rowCount() - 1, 1, new QTableWidgetItem("0"));
    }

    ui->imageList->resizeColumnsToContents();
    ui->imageList->setCurrentCell(0, 0);
    ui->imageProgress->setRange(0, ui->imageList->rowCount() - 1);
    ui->imageProgress->setValue(0);

    ui->statusbar->showMessage(QString("Finished loading images from %1.").arg(currentFolder), STATUSBAR_TIMEOUT);
}

void AnnotateApp::on_actionSave_Classes_triggered()
{
    if (ui->classList->rowCount() == 0)
    {
        ui->statusbar->showMessage("No classes to save.", STATUSBAR_TIMEOUT);
        return;
    }

    if (classFile.isEmpty() || classFile.isNull() || !QFile::exists(classFile))
    {
        on_actionSave_Classes_As_triggered();
    }

    else
    {
        writeClasses(classFile);
    }
}

void AnnotateApp::on_actionSave_Classes_As_triggered()
{
    if (ui->classList->rowCount() == 0)
    {
        ui->statusbar->showMessage("No classes to save.", STATUSBAR_TIMEOUT);
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this, "Save File", QDir::homePath());

    if (fileName.isEmpty() || fileName.isNull())
    {
        return;
    }

    writeClasses(fileName);
}

void AnnotateApp::writeClasses(const QString& fileName)
{
    QFile file(fileName);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
    {
        QMessageBox::critical(this, "Failed to Open Class File for Writing", QString("Failed to open '%1'.").arg(fileName));
        return;
    }

    QTextStream out(&file);

    for (int row = 0; row < ui->classList->rowCount(); row++)
    {
        out << ui->classList->item(row, 0)->text() << endl;
    }

    file.close();

    ui->statusbar->showMessage(QString("Classes written to '%1'.").arg(fileName), STATUSBAR_TIMEOUT);
}

void AnnotateApp::on_actionSave_Session_triggered()
{

}

void AnnotateApp::on_actionSave_Session_As_triggered()
{

}

void AnnotateApp::on_classList_itemSelectionChanged()
{
    ui->cmdRemoveClass->setEnabled(ui->classList->currentRow() >= 0);
}

void AnnotateApp::on_imageList_itemSelectionChanged()
{
    if (ui->imageList->currentRow() >= 0)
    {
        QString file("");
        int row = ui->imageList->currentRow();
        file.append(currentFolder);
        file.append(ui->imageList->item(row, 0)->text());

        QPixmap image(file);

        if (!image.isNull())
        {
            QSize imageSize = ui->image->size();
            imageSize -= QSize(10, 10);

            ui->image->setPixmap(image.scaled(imageSize, Qt::KeepAspectRatio, Qt::SmoothTransformation));
        }

        else
        {
            QString col2 = ui->imageList->item(row, 0)->text();

            QString prompt = QString("Column 1: %1\nColumn 2: %2\nRow: %3").arg(file).arg(col2).arg(row);

            QMessageBox::critical(this, "Critical Error", prompt);
        }
    }
}

void AnnotateApp::on_imageProgress_valueChanged(int value)
{
    if (ui->imageList->currentRow() != (value))
    {
        ui->imageList->setCurrentCell(value, 0);
        ui->imageList->scrollToItem(ui->imageList->currentItem(), QAbstractItemView::PositionAtTop);
    }
}

void AnnotateApp::keyPressEvent(QKeyEvent *event)
{
    bool ctrl      = (event->modifiers() == Qt::ControlModifier);
    bool ctrlShift = (event->modifiers() == Qt::ControlModifier | Qt::ShiftModifier);

    switch (event->key())
    {
        // Detect "save class" sequences.
        case Qt::Key_C:
        {
            if (ctrl)
            {
                on_actionSave_Classes_triggered();
            }

            else if (ctrlShift)
            {
                on_actionSave_Classes_As_triggered();
            }

            else
            {
                QWidget::keyPressEvent(event);
            }

            break;
        }

        // Detect "load directory" sequence.
        case Qt::Key_L:
        {
            if (ctrl)
            {
                on_actionLoad_Directory_triggered();
            }

            else
            {
                QWidget::keyPressEvent(event);
            }

            break;
        }

        // Detect "save sesion" sequences.
        case Qt::Key_S:
        {
            if (ctrl)
            {
                on_actionSave_Session_triggered();
            }

            else if (ctrlShift)
            {
                on_actionSave_Session_As_triggered();
            }

            else
            {
                QWidget::keyPressEvent(event);
            }

            break;
        }

        // Detect quit sequence.
        case Qt::Key_Q:
        {
            if (ctrl)
            {
                this->close();
            }

            else
            {
                QWidget::keyPressEvent(event);
            }

            break;
        }

        // Detect "change class" sequence.
        // This is accomplished by changing the active item in the class list.
        // Up = go up one class
        // Down = go down one class
        case Qt::Key_Up:
        case Qt::Key_Down:
        {
            int row = ui->classList->currentRow() + ((event->key() == Qt::Key_Up) ? -1 : 1);
            row = std::max(0, std::min(row, ui->classList->rowCount() - 1));
            ui->classList->setCurrentCell(row, 0);
            ui->classList->scrollToItem(ui->classList->currentItem(), QAbstractItemView::PositionAtTop);

            break;
        }

        // Detect "change image" sequence.
        // This is accomplished by changing the active item in the image list.
        // Left = previous image
        // Right = next image.
        case Qt::Key_Left:
        case Qt::Key_Right:
        {
            int row = ui->imageList->currentRow() + ((event->key() == Qt::Key_Left) ? -1 : 1);
            row = std::max(0, std::min(row, ui->imageList->rowCount() - 1));
            ui->imageList->setCurrentCell(row, 0);
            ui->imageList->scrollToItem(ui->imageList->currentItem(), QAbstractItemView::PositionAtTop);
            ui->imageProgress->setValue(ui->imageList->currentRow());

            break;
        }

        default:
        {
            QWidget::keyPressEvent(event);
        }
    }
}

void AnnotateApp::wheelEvent(QWheelEvent *event)
{

}

bool AnnotateApp::eventFilter(QObject *obj, QEvent *event)
{
    if ((obj == ui->imageList) || (obj == ui->classList))
    {
        if (event->type() == QEvent::KeyPress)
        {
            keyPressEvent(static_cast<QKeyEvent*>(event));
            return true;
        }

        else
        {
            return false;
        }
    }

    else
    {
        // pass the event on to the parent class
        return QMainWindow::eventFilter(obj, event);
    }
}

void AnnotateApp::mousePressEvent(QMouseEvent *event)
{
    // Create a rubber band if the mouse event is happening inside the image.
    if (ui->image->geometry().contains(event->pos()))
    {
        origin = event->pos();

        if (!rubberBand)
        {
            rubberBand = new QRubberBand(QRubberBand::Rectangle, this);
        }

        rubberBand->setGeometry(QRect(origin, QSize()));
        rubberBand->show();
    }
}

void AnnotateApp::mouseMoveEvent(QMouseEvent *event)
{
    // Only act if we actually have a rubber band to work with.
    if (rubberBand && rubberBand->isVisible())
    {
        // Check to see if the mouse is still inside the iamge or not.
        // If it is, then update the rubber band to the new coordinates.
        QRect fixedRect (ui->image->x(),
                         ui->image->y() + ui->menubar->height(),
                         ui->image->width(),
                         ui->image->height());

        if (fixedRect.contains(event->pos()))
        {
            rubberBand->setGeometry(QRect(origin, event->pos()).normalized());
        }

        // If it is not, then clamp the coordinates to the image boundary.
        else
        {
            int x = std::min(fixedRect.right(),  std::max(event->pos().x(), fixedRect.left()));
            int y = std::min(fixedRect.bottom(), std::max(event->pos().y(), fixedRect.top()));

            rubberBand->setGeometry(QRect(origin, QPoint(x, y)).normalized());
        }
    }
}

void AnnotateApp::mouseReleaseEvent(QMouseEvent *event)
{
    // Only act if we actually have a rubber band to work with.
    if (rubberBand && rubberBand->isVisible())
    {
        //rubberBand->hide();
        // determine selection, for example using QRect::intersects()
        // and QRect::contains().
    }
}
