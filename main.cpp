#include "AnnotateApp.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AnnotateApp w;
    w.show();

    return a.exec();
}
